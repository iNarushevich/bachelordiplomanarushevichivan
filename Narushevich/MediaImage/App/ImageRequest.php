<?php

namespace Narushevich\MediaImage\App;

use Magento\Framework\App;
use Magento\Framework\App\State;
use Magento\Framework\AppInterface;
use Magento\MediaStorage\App\MediaFactory;
use Magento\MediaStorage\Model\File\Storage\Response;
use Narushevich\MediaImage\Model\ImageRequest\ProcessorInterface;

/**
 * Class ImageRequest
 *
 * @package Narushevich\MediaImage\App
 */
class ImageRequest implements AppInterface
{
    private $mediaProcessor;
    private $mediaAppFactory;
    private $state;
    private $configCacheFile;
    private $mediaDirectory;
    private $response;

    public function __construct(
        $configCacheFile,
        Response $response,
        $mediaDirectory,
        ProcessorInterface $mediaProcessor,
        MediaFactory $mediaAppFactory,
        State $state
    ) {
        $this->mediaProcessor = $mediaProcessor;
        $this->mediaAppFactory = $mediaAppFactory;
        $this->state = $state;
        $this->configCacheFile = $configCacheFile;
        $this->mediaDirectory = $mediaDirectory;
        $this->response = $response;
    }

    public function launch()
    {
        $this->state->setAreaCode('frontend');
        $filePath = $this->mediaProcessor->calculatePath();

        $mediaApp = $this->mediaAppFactory->create([
            'mediaDirectory'  => $this->mediaDirectory,
            'configCacheFile' => $this->configCacheFile,
            'isAllowed'       => function ($resource, array $allowedResources) {
                foreach ($allowedResources as $allowedResource) {
                    if (0 === stripos($resource, $allowedResource)) {
                        return true;
                    }
                }
                return false;
            },
            'relativeFileName' => $filePath
        ]);
        return $mediaApp->launch();
    }

    public function catchException(App\Bootstrap $bootstrap, \Exception $exception)
    {
        $this->response->setHttpResponseCode(404);
        if ($bootstrap->isDeveloperMode()) {
            $this->response->setHeader('Content-Type', 'text/plain');
            $this->response->setBody($exception->getMessage() . "\n" . $exception->getTraceAsString());
        }
        $this->response->sendResponse();
        return true;
    }
}
