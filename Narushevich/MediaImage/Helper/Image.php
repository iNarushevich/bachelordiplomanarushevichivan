<?php

namespace Narushevich\MediaImage\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Narushevich\MediaImage\Model\Image as MediaImage;

/**
 * Class Image
 *
 * @package Narushevich\MediaImage\Helper
 */
class Image extends AbstractHelper
{
    const MEDIA_TYPE_CONFIG_NODE = 'images';

    private $attributes;
    private $viewConfig;
    private $configView;
    private $imageModel;
    private $imageFactory;
    private $path;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\View\ConfigInterface $viewConfig,
        \Narushevich\MediaImage\Model\ImageFactory $imageFactory
    ) {
        parent::__construct($context);
        $this->viewConfig = $viewConfig;
        $this->imageFactory = $imageFactory;
    }

    public function init(string $path, string $imageId = '', array $attributes = [])
    {
        $this->reset();

        $this->path = $path;
        $this->attributes = array_merge(
            $this->getConfigView()->getMediaAttributes(
                'Narushevich_MediaImage',
                self::MEDIA_TYPE_CONFIG_NODE,
                $imageId
            ),
            $attributes
        );
        $this->setImageProperties();

        return $this;
    }

    public function resize($width, $height = null)
    {
        $this
            ->getImageModel()
            ->setWidth($width)
            ->setHeight($height);
        return $this;
    }

    public function setQuality($quality)
    {
        $this->getImageModel()->setQuality($quality);
        return $this;
    }

    public function getUrl(): string
    {
        $image = $this->getImageModel();
        try {
            $image->setBaseFile($this->path);
            if (!$image->isCached()) {
                $image->resize();
                $image->saveFile();
            }
            return $image->getUrl();
        } catch (\Exception $e) {
            return '';
        }
    }

    private function reset()
    {
        $this->attributes = [];
        $this->imageModel = null;
        $this->path = null;
    }

    private function getImageModel(): MediaImage
    {
        if (!$this->imageModel) {
            $this->imageModel = $this->imageFactory->create();
        }
        return $this->imageModel;
    }

    private function getConfigView()
    {
        if (!$this->configView) {
            $this->configView = $this->viewConfig->getViewConfig();
        }
        return $this->configView;
    }

    private function setImageProperties()
    {
        $image = $this->getImageModel();
        $image->setWidth($this->attributes['width'] ?? null)
            ->setHeight($this->attributes['height'] ?? null);
    }
}
