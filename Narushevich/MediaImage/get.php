<?php
/**
 * Media image files entry point
 */

use Magento\Framework\App\Cache\Frontend\Factory;
use Magento\Framework\App\ObjectManagerFactory;

require '../app/bootstrap.php';

$mediaDirectory = null;
$configCacheFile = BP . '/var/resource_config.json';

$params = $_SERVER;
if (empty($mediaDirectory)) {
    $params[ObjectManagerFactory::INIT_PARAM_DEPLOYMENT_CONFIG] = [];
    $params[Factory::PARAM_CACHE_FORCED_OPTIONS] = ['frontend_options' => ['disable_save' => true]];
}

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $params);
$processor = $bootstrap->getObjectManager()->create('\Narushevich\MediaImage\Model\ImageRequest\Processor');

$app = $bootstrap->createApplication(
    '\Narushevich\MediaImage\App\ImageRequest',
    [
        'mediaDirectory'  => $mediaDirectory,
        'configCacheFile' => $configCacheFile,
        'mediaProcessor'  => $processor
    ]
);
$bootstrap->run($app);
