<?php
namespace Narushevich\MediaImage\Model;

use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Image
 *
 * @package Narushevich\MediaImage\Model
 */
class Image
{
    const CACHE_DIR = 'media_image_cache';

    // Default image properties
    private $quality = 80;
    private $width;
    private $height;
    private $keepAspectRatio = true;
    private $keepFrame = true;
    private $keepTransparency = true;
    private $constrainOnly = true;
    private $backgroundColor = [255, 255, 255];
    private $angle;
    private $watermarkFile;
    private $watermarkImageOpacity;
    private $watermarkPosition;
    private $watermarkWidth;
    private $watermarkHeight;

    private $storeManager;
    private $baseFile;
    private $imageFactory;
    private $processor;
    private $filesystem;
    private $mediaDir;
    private $newFile;

    public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Framework\Image\Factory $imageFactory,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->storeManager = $storeManager;
        $this->imageFactory = $imageFactory;
        $this->filesystem = $filesystem;
    }

    public function getUrl(): string
    {
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl . $this->newFile;
    }

    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    public function setQuality($quality)
    {
        $this->quality = $quality;
        return $this;
    }

    public function getNewFile()
    {
        return $this->newFile;
    }

    public function isCached(): bool
    {
        if ($this->newFile) {
            return $this->getMediaDir()->isFile($this->newFile);
        }
        return false;
    }

    public function resize()
    {
        if ($this->width !== null || $this->height !== null) {
            $this->getImageProcessor()->resize($this->width, $this->height);
        }
        return $this;
    }

    public function saveFile()
    {
        if (!$this->newFile) {
            return;
        }

        $filename = $this->getMediaDir()->getAbsolutePath($this->newFile);
        $this->getImageProcessor()->save($filename);
    }

    public function setBaseFile(string $path)
    {
        if (strpos($path, '/' !== 0)) {
            $path = "/{$path}";
        }

        if (!$this->getMediaDir()->isFile($path)) {
            return $this;
        }

        $cachePathParams = [ self::CACHE_DIR ];
        if (!empty($this->width) || !empty($this->height)) {
            $cachePathParams[] = "{$this->width}x{$this->height}";
        }
        $this->baseFile = $path;
        $cachePath = $this->getCachePath($cachePathParams);
        $this->newFile = $cachePath . $path;

        return $this;
    }

    public function getImageProcessor()
    {
        if (!$this->processor) {
            $filename = $this->baseFile ? $this->getMediaDir()->getAbsolutePath($this->baseFile) : null;
            $this->processor = $this->imageFactory->create($filename);
        }

        $this->processor->keepAspectRatio($this->keepAspectRatio);
        $this->processor->keepFrame($this->keepFrame);
        $this->processor->keepTransparency($this->keepTransparency);
        $this->processor->constrainOnly($this->constrainOnly);
        $this->processor->backgroundColor($this->backgroundColor);
        $this->processor->quality($this->quality);
        return $this->processor;
    }

    private function getMediaDir(): WriteInterface
    {
        if (!$this->mediaDir) {
            $this->mediaDir = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        }
        return $this->mediaDir;
    }

    private function rgbToString($rgb)
    {
        $result = [];
        foreach ($rgb as $value) {
            if (null === $value) {
                $result[] = 'null';
            } else {
                $result[] = sprintf('%02s', dechex($value));
            }
        }
        return implode($result);
    }

    private function getCachePath($pathParams): string
    {
        $miscParams = [
            ($this->keepAspectRatio ? '' : 'non') . 'proportional',
            ($this->keepFrame ? '' : 'no') . 'frame',
            ($this->keepTransparency ? '' : 'no') . 'transparency',
            ($this->constrainOnly ? 'do' : 'not') . 'constrainonly',
            $this->rgbToString($this->backgroundColor),
            'angle' . $this->angle,
            'quality' . $this->quality,
        ];
        if ($this->watermarkFile) {
            $miscParams[] = $this->watermarkFile;
            $miscParams[] = $this->watermarkImageOpacity;
            $miscParams[] = $this->watermarkPosition;
            $miscParams[] = $this->watermarkWidth;
            $miscParams[] = $this->watermarkHeight;
        }
        $pathParams[] = md5(implode('_', $miscParams));
        return implode('/', $pathParams);
    }
}