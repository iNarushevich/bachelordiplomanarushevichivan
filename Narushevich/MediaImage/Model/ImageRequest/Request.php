<?php

namespace Narushevich\MediaImage\Model\ImageRequest;

use Magento\Framework\HTTP\PhpEnvironment\Request as HttpRequest;

/**
 * Class Request
 *
 * @package Narushevich\MediaImage\Model\ImageRequest
 */
class Request
{
    private $request;

    public function __construct(HttpRequest $request)
    {
        $this->request = $request;
    }

    public function getPathInfo(): string
    {
        return str_replace('..', '', ltrim($this->request->getPathInfo(), '/'));
    }

    public function getImageId(): string
    {
        return (string) $this->request->getParam('image');
    }

    public function getStoreId()
    {
        return (string) $this->request->getParam('store');
    }
}