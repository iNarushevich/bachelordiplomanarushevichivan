<?php
namespace Narushevich\MediaImage\Model\ImageRequest;

use Narushevich\MediaImage\Model\Image;
use Narushevich\MediaImage\Model\ImageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\ConfigInterface;
use Magento\Framework\View\Design\Theme\ThemeProviderInterface;
use Magento\Framework\View\Design\ThemeInterface;

/**
 * Class Processor
 *
 * @package Narushevich\MediaImage\Model\ImageRequest
 */
class Processor implements ProcessorInterface
{
    private $request;
    private $productImageFactory;
    private $viewConfig;
    private $themeProvider;
    private $scopeConfig;

    public function __construct(
        Request $request,
        ImageFactory $productImageFactory,
        ScopeConfigInterface $scopeConfig,
        ConfigInterface $viewConfig,
        ThemeProviderInterface $themeProvider
    ) {
        $this->request = $request;
        $this->productImageFactory = $productImageFactory;
        $this->viewConfig = $viewConfig;
        $this->themeProvider = $themeProvider;
        $this->scopeConfig = $scopeConfig;
    }

    public function calculatePath()
    {
        $image = $this->initImage();

        if (!$image->isCached()) {
            $image->resize();
            $image->saveFile();
        }
        return $image->getNewFile();
    }

    private function initImage(): Image
    {
        $image = $this->productImageFactory->create();
        $attributes = $this->loadImageAttributes();

        if ($width = ($attributes['width'] ?? null)) {
            $image->setWidth($width);
        }
        if ($height = ($attributes['height'] ?? null)) {
            $image->setHeight($height);
        }

        $image->setBaseFile($this->request->getPathInfo());
        return $image;
    }

    private function loadTheme(): ThemeInterface
    {
        $store = $this->request->getStoreId();
        if (!$store) {
            throw new \Exception('Store not found');
        }
        $themeId = $this->scopeConfig->getValue(
            \Magento\Framework\View\DesignInterface::XML_PATH_THEME_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->request->getStoreId()
        );
        return $this->themeProvider->getThemeById($themeId);
    }

    private function loadImageAttributes(): array
    {
        $attributes = $this->viewConfig
            ->getViewConfig([
                'area'       => 'frontend',
                'themeModel' => $this->loadTheme()
            ])
            ->getMediaAttributes('Narushevich_MediaImage', 'images', $this->request->getImageId());

        if (!$attributes) {
            throw new \Exception('Image attributes not found');
        }

        return $attributes;
    }
}