<?php

namespace Narushevich\MediaImage\Model\ImageRequest;

/**
 * Interface ProcessorInterface
 *
 * @package Narushevich\MediaImage\Model\ImageRequest
 */
interface ProcessorInterface
{
    public function calculatePath();
}