<?php
namespace Narushevich\MediaImage\Model\Image;

use Magento\Framework\App\Filesystem\DirectoryList;
use Narushevich\MediaImage\Model\Image;

/**
 * Class Cache
 *
 * @package Narushevich\MediaImage\Model\Image
 */
class Cache
{
    private $filesystem;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->filesystem = $filesystem;
    }

    public function clean()
    {
        $media = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $media->delete(Image::CACHE_DIR);
    }
}