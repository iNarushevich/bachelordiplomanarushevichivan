<?php
namespace Narushevich\MediaImage\Model\View;

use Magento\Framework\View\ConfigInterface;
use Magento\Framework\View\Asset\Repository;

/**
 * Class Config
 *
 * @package Narushevich\MediaImage\Model\View
 */
class Config implements ConfigInterface
{
    private $assetRepo;
    private $viewConfigFactory;
    private $viewConfigs = [];

    const FILENAME = 'etc/media_images.xml';

    public function __construct(
        Repository $assetRepo,
        ViewFactory $viewConfigFactory
    ) {
        $this->assetRepo = $assetRepo;
        $this->viewConfigFactory = $viewConfigFactory;
    }

    public function getViewConfig(array $params = [])
    {
        $this->assetRepo->updateDesignParams($params);
        $viewConfigParams = [];

        if (isset($params['themeModel'])) {
            /** @var \Magento\Framework\View\Design\ThemeInterface $currentTheme */
            $currentTheme = $params['themeModel'];
            $key = $currentTheme->getFullPath();
            if (isset($this->viewConfigs[$key])) {
                return $this->viewConfigs[$key];
            }
            $viewConfigParams['themeModel'] = $currentTheme;
        }
        $viewConfigParams['area'] = (isset($params['area'])) ? $params['area'] : null;
        $viewConfigParams['fileName'] = self::FILENAME;

        /** @var \Magento\Framework\Config\View $config */
        $config = $this->viewConfigFactory->create($viewConfigParams);

        if (isset($params['themeModel'])) {
            $this->viewConfigs[$params['themeModel']->getFullPath()] = $config;
        }

        return $config;
    }
}