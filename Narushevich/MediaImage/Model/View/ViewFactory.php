<?php

namespace Narushevich\MediaImage\Model\View;

use Magento\Framework\ObjectManagerInterface;

/**
 * Class ViewFactory
 *
 * @package Narushevich\MediaImage\Model\View
 */
class ViewFactory
{
    private $objectManager;

    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Create new view object
     *
     * @param array $arguments
     * @return \Magento\Framework\Config\View
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create(array $arguments = [])
    {
        $viewConfigArguments = [];

        if (isset($arguments['themeModel']) && isset($arguments['area'])) {
            if (!($arguments['themeModel'] instanceof \Magento\Framework\View\Design\ThemeInterface)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    new \Magento\Framework\Phrase(
                        '%1 doesn\'t implement ThemeInterface', [$arguments['themeModel']]
                    )
                );
            }
            /** @var \Magento\Theme\Model\View\Design $design */
            $design = $this->objectManager->create('Magento\Theme\Model\View\Design');
            $design->setDesignTheme($arguments['themeModel'], $arguments['area']);
            /** @var \Magento\Framework\Config\FileResolver $fileResolver */
            $fileResolver = $this->objectManager->create(
                'Narushevich\MediaImage\Model\View\FileResolver',
                [
                    'designInterface' => $design,
                ]
            );
            $viewConfigArguments['fileResolver'] = $fileResolver;
        }

        if (isset($arguments['fileName'])) {
            $viewConfigArguments['fileName'] = $arguments['fileName'];
        }

        return $this->objectManager->create(
            'Magento\Framework\Config\View',
            $viewConfigArguments
        );
    }
}