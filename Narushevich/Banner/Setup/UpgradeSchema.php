<?php

namespace Narushevich\Banner\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\{
    ModuleContextInterface, SchemaSetupInterface, UpgradeSchemaInterface
};

/**
 * Class UpgradeSchema
 *
 * @package Narushevich\Banner\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.0', '<')) {
            $this->addBannerTitleFields($setup);
            $this->addBannerPackshotImageField($setup);
        }

        if (version_compare($context->getVersion(), '0.1.1', '<')) {
            $this->changeBannerButtonLabelField($setup);
        }

        if (version_compare($context->getVersion(), '0.1.2', '<')) {
            $this->addBannerTextColorField($setup);
        }

        if (version_compare($context->getVersion(), '0.1.3', '<')) {
            $this->addBannerRemoveButtonField($setup);
        }

        if (version_compare($context->getVersion(), '0.1.4', '<')) {
            $this->addBannerPlaceholderImageField($setup);
        }

        if (version_compare($context->getVersion(), '0.2.0', '<')) {
            $this->addCustomerGenderField($setup);
        }

        if (version_compare($context->getVersion(), '0.2.1', '<')) {
            $this->addCustomerAgesLimitsFields($setup);
        }

        if (version_compare($context->getVersion(), '0.2.2', '<')) {
            $this->addCustomerTypeField($setup);
        }

        $setup->endSetup();
    }

    private function addBannerTitleFields(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'title', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Title'
            ]
        );
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'subtitle', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Subtitle'
            ]
        );
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'btn_label', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Button label'
            ]
        );
    }

    private function addBannerPackshotImageField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'packshot_image', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Packshot image'
            ]
        );
    }

    private function addBannerPlaceholderImageField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'placeholder_image', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Placeholder Image'
            ]
        );
    }

    private function changeBannerButtonLabelField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->changeColumn(
            $setup->getTable('narushevich_banner'),
            'btn_label',
            'button_label',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Button label'
            ]
        );
    }

    private function addBannerTextColorField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'text_color', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'comment' => 'Text color'
            ]
        );
    }

    private function addBannerRemoveButtonField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'remove_button', [
                'type' => Table::TYPE_BOOLEAN,
                'comment' => 'Remove Button'
            ]
        );
    }

    private function addCustomerGenderField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'customer_gender', [
                'type' => Table::TYPE_SMALLINT,
                'comment' => 'Customer Gender'
            ]
        );
    }

    private function addCustomerAgesLimitsFields(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'customer_minimal_gender', [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'Customer Minimal gender'
            ]
        );
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'customer_maximal_gender', [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'Customer maximal gender'
            ]
        );
    }

    private function addCustomerTypeField(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->addColumn(
            $setup->getTable('narushevich_banner'), 'customer_type', [
                'type' => Table::TYPE_SMALLINT,
                'comment' => 'Customer Type'
            ]
        );
    }
}
