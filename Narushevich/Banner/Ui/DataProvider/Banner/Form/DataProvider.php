<?php

namespace Narushevich\Banner\Ui\DataProvider\Banner\Form;

use Narushevich\Banner\Model\Banner;

/**
 * Class DataProvider
 *
 * @package Narushevich\Banner\Ui\DataProvider\Banner\Form
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;
    protected $loadedData;
    protected $dataPersistor;
    private $storeManager;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Narushevich\Banner\Model\ResourceModel\Banner\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $bannerData = [];
        /** @var Banner $banner */
        foreach ($items as $banner) {
            $bannerData = $banner->getData();
            $this->fetchImageFields($banner, $bannerData);
            $this->loadedData[$banner->getId()] = $bannerData;
        }

        $data = $this->dataPersistor->get('banner');
        if (!empty($data)) {
            $banner = $this->collection->getNewEmptyItem();
            $banner->setData($bannerData);
            $this->loadedData[$banner->getId()] = $banner->getData();
            $this->dataPersistor->clear('banner');
        }

        return $this->loadedData;
    }

    private function fetchImageFields(Banner $bannerModel, array &$bannerData)
    {
        foreach ($bannerModel->getImageAttributes() as $imageType) {
            if (isset($bannerData[$imageType])) {
                unset($bannerData[$imageType]);
                $bannerData[$imageType][0]['name'] = $bannerModel->getData($imageType);
                $bannerData[$imageType][0]['url'] = $this->getImageUrl($bannerModel, $imageType);
            }
        }
    }

    private function getImageUrl(Banner $banner, $imageType)
    {
        $image = $banner->getData($imageType);
        if (!$image) {
            return false;
        }

        if (is_string($image)) {
            return $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . "banner/{$imageType}/$image";
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while getting the image url.')
            );
        }

    }
}
