<?php
namespace Narushevich\Banner\Controller\Adminhtml;

use Magento\Framework\View\Result\Page;

/**
 * Class Banner
 *
 * @package Narushevich\Banner\Controller\Adminhtml
 */
abstract class Banner extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Narushevich_Banner::banner';

    protected $resultPageFactory;
    protected $resultForwardFactory;
    protected $coreRegistry;
    protected $bannerFactory;
    protected $bannerRepository;
    protected $dataPersistor;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Narushevich\Banner\Model\BannerFactory $bannerFactory,
        \Narushevich\Banner\Api\BannerRepositoryInterface $bannerRepository,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->bannerFactory = $bannerFactory;
        $this->bannerRepository = $bannerRepository;
        $this->dataPersistor = $dataPersistor;
    }

    protected function initPage(Page $resultPage)
    {
        return $resultPage
            ->setActiveMenu('Narushevich_Banner::banner')
            ->addBreadcrumb(__('Banner'), __('Banner'));
    }

    /**
     * Init Banner Model
     *
     * @return \Narushevich\Banner\Model\Banner
     */
    protected function initBanner()
    {
        $id = (int) $this->getRequest()->getParam('banner_id');
        $banner = $this->bannerFactory->create();
        if ($id) {
            $banner = $this->bannerRepository->get($id);
        }
        $this->coreRegistry->register('current_banner', $banner);
        return $banner;
    }
}