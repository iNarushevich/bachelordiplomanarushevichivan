<?php

namespace Narushevich\Banner\Controller\Adminhtml\Banner\Image;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Narushevich\Banner\Api\Data\BannerInterface;

/**
 * Class Upload
 *
 * @package Narushevich\Banner\Controller\Adminhtml\Banner\Image
 */
class Upload extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Narushevich_Banner::banner';

    private $bannerHelper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Narushevich\Banner\Helper\Banner $bannerHelper
    ) {
        parent::__construct($context);
        $this->bannerHelper = $bannerHelper;
    }

    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $type = $this->getRequest()->getParam('type');
            $imageUploader = $this->bannerHelper->getImageUploader($type);
            $result = $imageUploader->saveFileToTmpDir($type);

            $result['cookie'] = [
                'name'     => $this->_getSession()->getName(),
                'value'    => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path'     => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
