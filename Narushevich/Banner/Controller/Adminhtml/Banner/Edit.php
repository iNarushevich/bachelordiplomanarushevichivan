<?php

namespace Narushevich\Banner\Controller\Adminhtml\Banner;

/**
 * Class Edit
 * @package Narushevich\Banner\Controller\Adminhtml\Banner
 */
class Edit extends \Narushevich\Banner\Controller\Adminhtml\Banner
{
    /**
     * Edit banner
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $banner = $this->initBanner();

        $id = $this->getRequest()->getParam('banner_id');

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Banner') : __('New Banner'),
            $id ? __('Edit Banner') : __('New Banner')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Banners'));
        $resultPage
            ->getConfig()
            ->getTitle()
            ->prepend($banner->getId() ? $banner->getName() : __('New Banner'));
        return $resultPage;
    }
}
