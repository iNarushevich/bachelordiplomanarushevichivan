<?php
namespace Narushevich\Banner\Controller\Adminhtml\Banner\Widget;

use Magento\Backend\App\Action;

/**
 * Class Chooser
 *
 * @package Narushevich\Banner\Controller\Adminhtml\Banner\Widget
 */
class Chooser extends Action
{
    public function execute()
    {
        $bannersGrid = $this->_view->getLayout()->createBlock(
            'Narushevich\Banner\Block\Adminhtml\Widget\Chooser\Grid'
        );
        $html = $bannersGrid->toHtml();
        $this->getResponse()->setBody($html);
    }
}