<?php

namespace Narushevich\Banner\Controller\Adminhtml\Banner;

/**
 * Class NewAction
 *
 * @package Narushevich\Banner\Controller\Adminhtml\Banner
 */
class NewAction extends \Narushevich\Banner\Controller\Adminhtml\Banner
{
    /**
     * Create new banner action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
