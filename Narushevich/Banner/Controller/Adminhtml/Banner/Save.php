<?php
namespace Narushevich\Banner\Controller\Adminhtml\Banner;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 *
 * @package Narushevich\Banner\Controller\Adminhtml\Banner
 */
class Save extends \Narushevich\Banner\Controller\Adminhtml\Banner
{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $formData = $this->getRequest()->getPostValue();
        if ($formData) {
            $id = $this->getRequest()->getParam('banner_id');
            $bannerData = $formData;

            if (empty($bannerData['banner_id'])) {
                $bannerData['banner_id'] = null;
            }
            $banner = $this->initBanner();

            if (!$banner->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This banner no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $bannerData = $this->processImages($banner, $bannerData);
            $banner->addData($this->filterBannerPostData($banner, $bannerData));

            try {
                $this->bannerRepository->save($banner);
                $this->messageManager->addSuccessMessage(__('You saved the banner.'));
                $this->dataPersistor->clear('banner');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['banner_id' => $banner->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the banner.'));
            }

            $this->dataPersistor->set('banner', $formData);
            return $resultRedirect->setPath('*/*/edit', [
                    'banner_id' => $this->getRequest()->getParam('banner_id')
                ]
            );
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function processImages(\Narushevich\Banner\Model\Banner $banner, $data)
    {
        foreach ($banner->getImageAttributes() as $attribute) {
            if (empty($data[$attribute])) {
                unset($data[$attribute]);
                $data[$attribute]['delete'] = true;
            }
        }
        return $data;
    }

    protected function filterBannerPostData(\Narushevich\Banner\Model\Banner $banner, array $rawData)
    {
        $data = $rawData;
        foreach ($banner->getImageAttributes() as $attribute) {
            if (isset($data[$attribute]) && is_array($data[$attribute])) {
                if (!empty($data[$attribute]['delete'])) {
                    $data[$attribute] = null;
                } else {
                    if (isset($data[$attribute][0]['name']) && isset($data[$attribute][0]['tmp_name'])) {
                        $data[$attribute] = $data[$attribute][0]['name'];
                    } else {
                        unset($data[$attribute]);
                    }
                }
            }
        }
        return $data;
    }
}
