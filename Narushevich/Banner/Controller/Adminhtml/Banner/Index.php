<?php
namespace Narushevich\Banner\Controller\Adminhtml\Banner;

use Narushevich\Banner\Controller\Adminhtml\Banner;

/**
 * Class Index
 *
 * @package Narushevich\Banner\Controller\Adminhtml\Banner
 */
class Index extends Banner
{
    public function execute()
    {
        $page = $this->resultPageFactory->create();
        $this
            ->initPage($page)
            ->getConfig()
            ->getTitle()
            ->prepend(__('Banners'));

        return $page;
    }
}