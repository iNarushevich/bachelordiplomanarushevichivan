<?php
namespace Narushevich\Banner\Plugin;

use Magento\Framework\Api\SearchCriteriaInterface;
use Narushevich\Banner\Api\PageBannerServiceInterface;
use Narushevich\Banner\Model\Page\CategoryPage;

/**
 * Class CategoryProductRepositoryPlugin
 *
 * @package Narushevich\Banner\Plugin
 */
class CategoryProductRepositoryPlugin
{
    private $pageBannerService;
    private $categoryPage;

    public function __construct(
        PageBannerServiceInterface $pageBannerService,
        CategoryPage $categoryPage
    ) {
        $this->pageBannerService = $pageBannerService;
        $this->categoryPage = $categoryPage;
    }

    public function aroundGetCategoryProducts(
        \Closure $proceed,
        $categoryId,
        $searchCriteria = null
    ) {
        $result = $proceed($categoryId, $searchCriteria);
        $extension = $result->getExtensionAttributes();

        $categoryBanners = $this->pageBannerService->getBanners(
            $this->categoryPage->getLayoutHandles($categoryId)
        );
        $extension->setBanners($categoryBanners);

        $result->setExtensionAttributes($extension);
        return $result;
    }
}
