<?php
namespace Narushevich\Banner\Block\Widget;

use Magento\Framework\View\Element\Template\Context;
use Narushevich\Banner\Api\Data\BannerInterface;
use Narushevich\Banner\Model\ResourceModel\Banner\CollectionFactory;

/**
 * Class Banner
 * @package Narushevich\Banner\Block\Widget
 */
class Banner
    extends \Magento\Framework\View\Element\Template
    implements \Magento\Widget\Block\BlockInterface
{
    const PDP_CONTAINER_NAME = 'product.view.promo.banners';

    private $collectionFactory;
    private $banners;
    private $contentFilterProvider;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        \Magento\Cms\Model\Template\FilterProvider $contentFilterProvider,
        $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->contentFilterProvider = $contentFilterProvider;
    }

    /**
     * @return BannerInterface[]
     */
    public function getBanners()
    {
        if (!$this->getVisibility()) {
            return [];
        }
        if ($this->banners === null) {
            $collection = $this->collectionFactory->create();

            $positions = $this->getBannersConfig();
            asort($positions);
            $collection
                ->filterByIds(array_keys($this->getBannersConfig()))
                ->filterByCustomersData()
                ->filterByActive()
                ->sortByPositions(array_keys($positions));

            $this->banners = $collection->getItems();
        }
        return $this->banners;
    }

    /**
     * Return widget banners configuration:
     * [
     *   banner_id => position
     *   banner_id => position
     *   ...
     * ]
     *
     * @return array
     */
    public function getBannersConfig()
    {
        return (array) \Zend\Json\Decoder::decode(
            $this->getData('banners'), \Zend\Json\Json::TYPE_ARRAY
        );
    }

    public function getBannerContent(BannerInterface $banner)
    {
        return $this->contentFilterProvider->getBlockFilter()->filter(
            $banner->getContent()
        );
    }

    public function getBannerPosition(BannerInterface $banner)
    {
        return $this->getBannersConfig()[$banner->getBannerId()] ?? 0;
    }

    public function getVisibility()
    {
        if (!$this->getIsSingle()) {
            return true;
        }
        $sortValue = $this->getSort();
        $siblings = $this->getLayout()->getStructure()->getElement(self::PDP_CONTAINER_NAME)['children'] ?? [];
        foreach ($siblings as $sibling) {
            if ($sibling == $this->getNameInLayout()) {
                continue;
            }
            $sibling = $this->getLayout()->getBlock($sibling);
            if ($sibling->getSort() < $sortValue) {
                return false;
            }
        }
        return true;
    }
}