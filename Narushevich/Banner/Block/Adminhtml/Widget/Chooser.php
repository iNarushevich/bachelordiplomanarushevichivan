<?php
namespace Narushevich\Banner\Block\Adminhtml\Widget;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Element\Template;

/**
 * Class Chooser
 *
 * @package Narushevich\Banner\Block\Adminhtml\Widget
 */
class Chooser extends Template
{
    protected $_template = 'widget/chooser.phtml';
    private $blockGrid;
    private $bannersJson;
    private $element;

    public function getBlockGrid(): Chooser\Grid
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                Chooser\Grid::class,
                'widget.banners.grid'
            );

            $this->blockGrid->setBannersJson($this->bannersJson);
        }
        return $this->blockGrid;
    }

    public function getGridHtml() : string
    {
        return $this->getBlockGrid()->toHtml();
    }

    public function getBannersJson() : string
    {
        return $this->bannersJson;
    }

    public function getGridJsFormName() : string
    {
        return $this->getBlockGrid()->getJsObjectName();
    }

    public function prepareElementHtml(AbstractElement $element)
    {
        $this->bannersJson = $element->getValue() ? : '{}';
        $this->element = $element;

        $element->setValue(null);
        $element->setData('after_element_html', $this->toHtml());
        return $element;
    }

    public function getElement()
    {
        return $this->element;
    }
}