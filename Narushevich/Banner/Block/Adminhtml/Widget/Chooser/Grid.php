<?php

namespace Narushevich\Banner\Block\Adminhtml\Widget\Chooser;

use Zend\Json\Decoder;
use Zend\Json\Json;

/**
 * Class Grid
 *
 * @package Narushevich\Banner\Block\Adminhtml\Widget\Chooser
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    private $collectionFactory;
    private $bannersJson;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Narushevich\Banner\Model\ResourceModel\Banner\CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    )
    {
        parent::__construct($context, $backendHelper, $data);
        $this->collectionFactory = $collectionFactory;
    }

    public function setBannersJson($bannersJson)
    {
        $this->bannersJson = $bannersJson;
    }

    private function getBannersJson()
    {
        $banners = $this->getRequest()->getParam('banners_json');
        if ($banners === null) {
            return $this->bannersJson;
        }
        return $banners;
    }

    protected function _construct()
    {
        $this
            ->setId('widget_banner')
            ->setDefaultSort('banner_id')
            ->setDefaultFilter(['in_widget' => 1])
            ->setUseAjax(true);
        parent::_construct();
    }

    private function getBannersConfig(): array
    {
        return (array)Decoder::decode($this->getBannersJson(), Json::TYPE_ARRAY);
    }

    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->create();
        $this->setCollection($collection);
        $result = parent::_prepareCollection();

        $config = $this->getBannersConfig();
        foreach ($this->getCollection() as $banner) {
            $banner->setPosition($config[$banner->getId()] ?? null);
        }
        return $result;
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_widget',
            [
                'type'             => 'checkbox',
                'name'             => 'in_widget',
                'values'           => $this->getSelectedBanners(),
                'index'            => 'banner_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'banner_id',
            [
                'header'           => __('ID'),
                'sortable'         => true,
                'index'            => 'banner_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);
        $this->addColumn(
            'position',
            [
                'header'   => __('Position'),
                'type'     => 'number',
                'index'    => 'position',
                'editable' => true,
                'sortable' => false,
                'filter'   => false
            ]
        );

        return parent::_prepareColumns();
    }

    private function getSelectedBanners()
    {
        return array_keys($this->getBannersConfig());
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_widget') {
            $bannerIds = $this->getSelectedBanners();
            if (empty($bannerIds)) {
                $bannerIds = [0];
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->filterByIds($bannerIds);
            } elseif (!empty($bannerIds)) {
                $this->getCollection()->excludeIds($bannerIds);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl(
            'banner/banner_widget/chooser',
            ['_current' => true, 'banners_json' => $this->getBannersJson()]
        );
    }
}