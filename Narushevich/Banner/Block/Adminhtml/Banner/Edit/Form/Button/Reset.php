<?php
namespace Narushevich\Banner\Block\Adminhtml\Banner\Edit\Form\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Reset
 *
 * @package Narushevich\Banner\Block\Adminhtml\Banner\Edit\Form\Button
 */
class Reset implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label'      => __('Reset'),
            'class'      => 'reset',
            'on_click'   => 'location.reload();',
            'sort_order' => 30
        ];
    }
}
