<?php
namespace Narushevich\Banner\Block\Adminhtml\Banner\Edit\Form\Button;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Generic
 * @package Narushevich\Banner\Block\Adminhtml\Banner\Edit\Form\Button
 */
class Generic
{
    protected $context;
    protected $bannerRepository;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Narushevich\Banner\Api\BannerRepositoryInterface $bannerRepository
    ) {
        $this->context = $context;
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Return banner Banner ID
     *
     * @return int|null
     */
    public function getBannerId()
    {
        try {
            return $this->bannerRepository->get(
                $this->context->getRequest()->getParam('banner_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
