/* global picturefill */

define([
    'ko',
    'jquery',
    'lazy',
    'slick'
],
function (ko, $, lazy) {
    'use strict';

    return function (options, element) {
        var $banner = $(element),
            isBannerChange = false;

        $banner.slick({
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            dots: true,
            arrows: true,
            autoplay: (options.autoplay !== undefined) ? options.autoplay : false,
            autoplaySpeed: options.autoplaySpeed || 3000,
            infinite: true,
            speed: 700,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });

        $banner.on('init', function () {
            lazy();
        });

        $banner.on('beforeChange', function () {
            if (!isBannerChange) {
                isBannerChange = true;

                $banner.find('.js-home-banner-item').each(function (index, item) {
                    var $item     = $(item),
                        $imgBig   = $item.find('.js-home-banner-big'),
                        $imgSmall = $item.find('.js-home-banner-small'),
                        srcBig    = $imgBig.attr('data-src'),
                        srcSmall  = $imgSmall.attr('data-src');

                    if (!srcBig && !srcSmall) {
                        return;
                    }


                    var picture = '<picture>' +
                        '<source class="js-home-banner-big" srcset="' + srcBig + '" media="(min-width: 768px)">' +
                        '<img class="js-home-banner-small" srcset="' + srcSmall + '" alt="">' +
                        '</picture>';

                    $imgBig.remove();
                    $imgSmall.remove();
                    $item.find('.js-home-banner-image').append(picture);
                    picturefill();

                }.bind(this));
            }
        }.bind(this));

        if (options.deferredAutoplay) {
            $(document).one('mousemove scroll', function () {
                $banner.slick('slickSetOption', 'autoplay', true, true)
            });
        }
    };
});
