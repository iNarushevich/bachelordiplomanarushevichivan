/* global $, $H */

define([
    'mage/adminhtml/grid'
], function () {
    'use strict';

    return function (config) {
        var selectedBanners = config.selectedBanners,
            widgetBanners = $H(selectedBanners),
            gridJsObject = window[config.gridJsObjectName],
            tabIndex = 1000;

        $('in_widget_banners').value = Object.toJSON(widgetBanners);

        /**
         * Register Category Product
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerWidgetBanner(grid, element, checked) {
            if (checked) {
                if (element.qtyElement) {
                    element.qtyElement.disabled = false;
                    widgetBanners.set(element.value, element.qtyElement.value);
                }
            } else {
                if (element.qtyElement) {
                    element.qtyElement.disabled = true;
                }
                widgetBanners.unset(element.value);
            }
            $('in_widget_banners').value = Object.toJSON(widgetBanners);
            grid.reloadParams = {
                'selected_banners[]': widgetBanners.keys()
            };
        }

        /**
         * Click on product row
         *
         * @param {Object} grid
         * @param {String} event
         */
        function widgetBannerRowClick(grid, event) {
            var trElement = Event.findElement(event, 'tr'),
                isInput = Event.element(event).tagName === 'INPUT',
                checked = false,
                checkbox = null;

            if (trElement) {
                checkbox = Element.getElementsBySelector(trElement, 'input');

                if (checkbox[0]) {
                    checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    gridJsObject.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }

        /**
         * Change banner position
         *
         * @param {String} event
         */
        function positionChange(event) {
            var element = Event.element(event);

            if (element && element.checkboxElement && element.checkboxElement.checked) {
                widgetBanners.set(element.checkboxElement.value, element.value);
                $('in_widget_banners').value = Object.toJSON(widgetBanners);
            }
        }

        /**
         * Initialize banner row
         *
         * @param {Object} grid
         * @param {String} row
         */
        function widgetBannerRowInit(grid, row) {
            var checkbox = $(row).getElementsByClassName('checkbox')[0],
                position = $(row).getElementsByClassName('input-text')[0];

            if (checkbox && position) {
                checkbox.qtyElement = position;
                position.checkboxElement = checkbox;
                position.disabled = !checkbox.checked;
                position.tabIndex = tabIndex++;
                Event.observe(position, 'keyup', positionChange);
            }
        }

        gridJsObject.rowClickCallback = widgetBannerRowClick;
        gridJsObject.initRowCallback = widgetBannerRowInit;
        gridJsObject.checkboxCheckCallback = registerWidgetBanner;

        if (gridJsObject.rows) {
            gridJsObject.rows.each(function (row) {
                widgetBannerRowInit(gridJsObject, row);
            });
        }
    };
});
