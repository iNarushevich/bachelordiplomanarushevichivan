<?php
namespace Narushevich\Banner\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Narushevich\Banner\Api\BannerRepositoryInterface;
use Narushevich\Banner\Api\Data\BannerInterface;
use Narushevich\Banner\Model\ResourceModel\Banner as ResourceModel;

/**
 * Class BannerRepository
 *
 * @package Narushevich\Banner\Model
 */
class BannerRepository implements BannerRepositoryInterface
{
    private $bannerFactory;
    private $resourceModel;

    public function __construct(
        \Narushevich\Banner\Model\BannerFactory $bannerFactory,
        ResourceModel $resourceModel
    ) {
        $this->bannerFactory = $bannerFactory;
        $this->resourceModel = $resourceModel;
    }

    public function get($id): BannerInterface
    {
        $banner = $this->bannerFactory->create();
        $this->resourceModel->load($banner, $id);

        if (!$banner->getId()) {
            throw new NoSuchEntityException(__('Banner not found'));
        }
        return $banner;
    }

    public function save(BannerInterface $banner)
    {
        $this->resourceModel->save($banner);
    }

    public function delete(BannerInterface $banner)
    {
        try {
            $this->resourceModel->delete($banner);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
    }
}
