<?php
namespace Narushevich\Banner\Model\ResourceModel\Banner\Grid;

use Narushevich\Banner\Model\ResourceModel\Banner;
use Narushevich\Banner\Model\ResourceModel\Banner\Collection as GridCollection;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Class Collection
 *
 * @package Narushevich\Banner\Model\ResourceModel\Banner\Grid
 */
class Collection extends GridCollection implements SearchResultInterface
{
    protected $aggregations;

    public function _construct()
    {
        $this->_init(Document::class, Banner::class);
    }

    public function getAggregations()
    {
        return $this->aggregations;
    }

    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

    public function getAllIds($limit = null, $offset = null)
    {
        return $this->getConnection()->fetchCol($this->_getAllIdsSelect($limit, $offset), $this->_bindParams);
    }

    public function getSearchCriteria()
    {
        return null;
    }

    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    public function setItems(array $items = null)
    {
        return $this;
    }
}
