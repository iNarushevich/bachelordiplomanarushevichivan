<?php
namespace Narushevich\Banner\Model\ResourceModel\Banner;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Payment\Gateway\Http\Client\Zend;
use Narushevich\Banner\Model\Banner;
use Narushevich\Banner\Model\ResourceModel\Banner as BannerResource;
use Narushevich\Banner\Model\ResourceModel\Helper;
use Narushevich\Banner\Model\Source\CustomerType;
use Narushevich\Banner\Model\Source\CustomerGenders;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

/**
 * Class Collection
 *
 * @package Narushevich\Banner\Model\ResourceModel\Banner
 */
class Collection extends AbstractCollection
{
    /** @var Helper */
    private $resourceHelper;

    /** @var CurrentCustomer */
    private $currentCustomer;

    /** @var CollectionFactory */
    private $orderCollectionFactory;

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        Helper $resourceHelper,
        CurrentCustomer $currentCustomer,
        CollectionFactory $orderCollectionFactory,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->resourceHelper    = $resourceHelper;
        $this->currentCustomer   = $currentCustomer;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    protected function _construct()
    {
        $this->_init(Banner::class, BannerResource::class);
    }

    public function filterByIds(array $ids)
    {
        return $this->addFieldToFilter('banner_id', ['in' => $ids]);
    }

    public function excludeIds(array $ids)
    {
        return $this->addFieldToFilter('banner_id', ['nin' => $ids]);
    }

    public function sortByPositions(array $sortedIds)
    {
        $this->getSelect()->order(
            $this->resourceHelper->getFieldExpr('banner_id', $sortedIds)
        );
        return $this;
    }

    public function filterByActive()
    {
        return $this->addFieldToFilter('is_active', 1);
    }

    /**
     * Filter colleciton by customers Data
     *
     * @return $this
     */
    public function filterByCustomersData()
    {
        $customerId = $this->currentCustomer->getCustomerId();
        if (!$customerId) {
            $this->filterByCustomerType(CustomerType::NEW_CUSTOMER_VALUE);
            $this->filterByGender(CustomerGenders::ANY_GENDER_VALUE);
            return $this;
        }
        $customer = $this->currentCustomer->getCustomer();
        $this->filterByCustomerType(null, $customer->getEmail());
        if ($gender = $customer->getGender()) {
            $this->filterByGender($gender);
        }
        if ($dob = $customer->getDob()) {
            $this->filterByCustomerAge($dob);
        }

        return $this;
    }

    /**
     * Filter collection by customer gender
     *
     * @param int $customerGender customer gender
     * @return Collection
     */
    public function filterByGender(int $customerGender)
    {
        return $this->addFieldToFilter('customer_gender', [$customerGender, CustomerGenders::ANY_GENDER_VALUE]);
    }

    /**
     * Fitler collection by customer type
     *
     * @param null|int $customerType
     * @param string $customerEmail
     * @return $this
     */
    public function filterByCustomerType($customerType = null, $customerEmail = '')
    {
        if ($customerType) {
            return $this->addFieldToFilter('customer_type', [$customerType, CustomerType::ANY_CUSTOMER_VALUE]);
        }
        if (!$customerEmail) {
            return $this;
        }
        $orders = $this->orderCollectionFactory->create()->addAttributeToFilter(
            'customer_email',
            $customerEmail
        );
        $customerType = CustomerType::NEW_CUSTOMER_VALUE;
        if ($orders->getSize()) {
            $customerType = CustomerType::WITH_ORDERS_CUSTOMER_VALUE;
        }

        return $this->addFieldToFilter('customer_type', [$customerType, CustomerType::ANY_CUSTOMER_VALUE]);
    }

    /**
     * Filter collection by customer age
     *
     * @param string $customerDob
     * @return $this
     */
    public function filterByCustomerAge(string $customerDob)
    {
        $dateString = $dateValue = strtotime($customerDob);
        $year = date("Y", $dateString);
        $customerAge = date("Y") - $year;
        if ($customerAge) {
            $this->addFieldToFilter(
                ['customer_minimal_gender', 'customer_minimal_gender'],
                [
                    ['eq' => 0],
                    ['gteq' => $customerAge]
                ]
            );
            $this->addFieldToFilter(
                ['customer_minimal_gender', 'customer_minimal_gender'],
                [
                    ['eq' => 0],
                    ['lteq' => $customerAge]
                ]
            );
        }
        return $this;
    }
}
