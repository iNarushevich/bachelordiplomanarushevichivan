<?php
namespace Narushevich\Banner\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Banner
 *
 * @package Narushevich\Banner\Model\ResourceModel
 */
class Banner extends AbstractDb
{
    private $bannerHelper;
    private $logger;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Narushevich\Banner\Helper\Banner $bannerHelper,
        \Psr\Log\LoggerInterface $logger,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $connectionName
        );
        $this->bannerHelper = $bannerHelper;
        $this->logger = $logger;
    }

    protected function _construct()
    {
        $this->_init('narushevich_banner', 'banner_id');
    }

    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        /** @var $object \Narushevich\Banner\Model\Banner */
        foreach ($object->getImageAttributes() as $attribute) {
            $image = $object->getData($attribute);
            if ($image !== null) {
                try {
                    $this->bannerHelper->getImageUploader($attribute)->moveFileFromTmp($image);
                } catch (\Exception $e) {
                    $this->logger->critical($e);
                }
            }
        }

        return parent::_afterSave($object);
    }
}