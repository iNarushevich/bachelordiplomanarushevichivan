<?php
namespace Narushevich\Banner\Model\ResourceModel;
/**
 * Class Helper
 *
 * @package Narushevich\Banner\Model\ResourceModel
 */
class Helper extends \Magento\Framework\DB\Helper
{
    public function __construct(\Magento\Framework\App\ResourceConnection $resource, $modulePrefix = 'Narushevich_Banner')
    {
        parent::__construct($resource, $modulePrefix);
    }

    public function getFieldExpr($field, array $list): \Zend_Db_Expr
    {
        $quotedField = $this->getConnection()->quoteIdentifier($field);
        $quotedList = implode(',', array_map(function ($str) {
            return $this->getConnection()->quote($str);
        }, $list));
        return new \Zend_Db_Expr("FIELD({$quotedField}, {$quotedList})");
    }
}