<?php
namespace Narushevich\Banner\Model\ResourceModel\Widget;

use Magento\Widget\Model\ResourceModel\Widget\Instance as WidgetInstance;

/**
 * Class Instance
 *
 * @package Narushevich\Banner\Model\ResourceModel\Widget
 */
class Instance
{
    private $instanceResource;

    public function __construct(WidgetInstance $instanceResource)
    {
        $this->instanceResource = $instanceResource;
    }

    /**
     * Get widget instances referenced to pages
     *
     * @param array $layoutHandles
     *
     * @return array
     */
    public function getInstancePages(array $layoutHandles): array
    {
        $conn = $this->instanceResource->getConnection();

        $pageLayoutSelect = $conn->select()
            ->distinct()
            ->from(
                ['pl' => $this->instanceResource->getTable('widget_instance_page_layout')],
                ['page_id']
            )
            ->join(
                ['lu' => $this->instanceResource->getTable('layout_update')],
                'lu.layout_update_id = pl.layout_update_id',
                []
            )
            ->where('lu.handle IN (?)', $layoutHandles);

        $select = $conn
            ->select()
            ->from(['page' => $this->instanceResource->getTable('widget_instance_page')])
            ->join(['layout' => $pageLayoutSelect], 'layout.page_id = page.page_id', []);

        return $conn->fetchAll($select);
    }
}