<?php
namespace Narushevich\Banner\Model\Page;

use Magento\Framework\DataObject;
use Narushevich\Banner\Api\Data\WidgetBannerInterface;
use Narushevich\Banner\Api\Data\WidgetInterface;

/**
 * Class Widget
 *
 * @package Narushevich\Banner\Model\Page
 */
class Widget
    extends DataObject
    implements WidgetInterface
{
    /**
     * @return WidgetBannerInterface[]
     */
    public function getBanners(): array
    {
        return $this->getData(self::BANNERS);
    }

    /**
     * @param WidgetBannerInterface[] $banners
     *
     * @return mixed
     */
    public function setBanners(array $banners)
    {
        return $this->setData(self::BANNERS, $banners);
    }
}