<?php
namespace Narushevich\Banner\Model\Page;

use Magento\Framework\DataObject;
use Narushevich\Banner\Api\Data\WidgetContainerInterface;
use Narushevich\Banner\Api\Data\WidgetInterface;

/**
 * Class WidgetContainer
 *
 * @package Narushevich\Banner\Model\Page
 */
class WidgetContainer
    extends DataObject
    implements WidgetContainerInterface
{
    /**
     * @return string
     */
    public function getContainer(): string
    {
        return $this->getData(self::CONTAINER);
    }

    /**
     * @return WidgetInterface[]
     */
    public function getWidgets(): array
    {
        return $this->getData(self::WIDGETS);
    }

    /**
     * @param string $container
     *
     * @return mixed
     */
    public function setContainer(string $container)
    {
        return $this->setData(self::CONTAINER, $container);
    }

    /**
     * @param array $widgets
     *
     * @return mixed
     */
    public function setWidgets(array $widgets)
    {
        return $this->setData(self::WIDGETS, $widgets);
    }
}