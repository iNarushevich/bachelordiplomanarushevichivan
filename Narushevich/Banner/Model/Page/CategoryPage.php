<?php
namespace Narushevich\Banner\Model\Page;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;

/**
 * Class CategoryPage
 *
 * @package Narushevich\Banner\Model\Page
 */
class CategoryPage
{
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getLayoutHandles($categoryId): array
    {
        $handles =  ['default', 'catalog_category_view'];
        if (!$categoryId) {
            return $handles;
        }

        $category = $this->categoryRepository->get($categoryId);
        if (!$category instanceof Category) {
            return $handles;
        }
        $type = $category->getIsAnchor() ? 'layered' : 'default';
        $handles[] = "catalog_category_view_type_{$type}";
        $handles[] = "catalog_category_view_id_{$categoryId}";

        return $handles;
    }
}