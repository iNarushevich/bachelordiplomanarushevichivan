<?php
namespace Narushevich\Banner\Model\Page;

use Magento\Framework\DataObject;
use Narushevich\Banner\Api\Data\BannerInterface;
use Narushevich\Banner\Api\Data\WidgetBannerInterface;

/**
 * Class WidgetBanner
 *
 * @package Narushevich\Banner\Model\Page
 */
class WidgetBanner
    extends DataObject
    implements WidgetBannerInterface
{
    /**
     * @return \Narushevich\Banner\Api\Data\BannerInterface
     */
    public function getBanner(): BannerInterface
    {
        return $this->getData(self::BANNER);
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->getData(self::POSITION);
    }

    /**
     * @param \Narushevich\Banner\Api\Data\BannerInterface $banner
     *
     * @return mixed
     */
    public function setBanner(\Narushevich\Banner\Api\Data\BannerInterface $banner)
    {
        return $this->setData(self::BANNER, $banner);
    }

    /**
     * @param int $position
     *
     * @return mixed
     */
    public function setPosition(int $position)
    {
        return $this->setData(self::POSITION, $position);
    }
}