<?php
namespace Narushevich\Banner\Model\Page;

use Magento\Widget\Model\ResourceModel\Widget\Instance\Collection;
use Magento\Widget\Model\Widget\Instance as WidgetInstance;
use Narushevich\Banner\Api\Data\WidgetContainerInterface;
use Narushevich\Banner\Api\Data\WidgetContainerInterfaceFactory;
use Narushevich\Banner\Api\Data\WidgetInterfaceFactory;
use Narushevich\Banner\Api\Data\WidgetBannerInterfaceFactory;
use Narushevich\Banner\Api\PageBannerServiceInterface;
use Narushevich\Banner\Model\ResourceModel\Banner\CollectionFactory as BannerCollectionFactory;
use Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory as WidgetInstanceCollectionFactory;
use Narushevich\Banner\Block\Widget\Banner;
use Narushevich\Banner\Model\ResourceModel\Widget\Instance;

/**
 * Class BannerService
 *
 * @package Narushevich\Banner\Model\Page
 */
class BannerService implements PageBannerServiceInterface
{
    private $instanceCollectionFactory;
    private $instanceResource;
    private $widgetContainerFactory;
    private $widgetFactory;
    private $bannerCollectionFactory;
    private $widgetBannerFactory;

    public function __construct(
        WidgetInstanceCollectionFactory $widgetInstanceCollectionFactory,
        WidgetContainerInterfaceFactory $widgetContainerFactory,
        WidgetInterfaceFactory $widgetFactory,
        BannerCollectionFactory $bannerCollectionFactory,
        WidgetBannerInterfaceFactory $widgetBannerFactory,
        Instance $instanceResource
    ) {
        $this->instanceCollectionFactory = $widgetInstanceCollectionFactory;
        $this->instanceResource = $instanceResource;
        $this->widgetContainerFactory = $widgetContainerFactory;
        $this->widgetFactory = $widgetFactory;
        $this->bannerCollectionFactory = $bannerCollectionFactory;
        $this->widgetBannerFactory = $widgetBannerFactory;
    }

    /**
     * @param string[] $layoutHandles
     * @return \Narushevich\Banner\Api\Data\WidgetContainerInterface[]
     */
    public function getBanners(array $layoutHandles): array
    {
        $instancePages = $this->instanceResource->getInstancePages($layoutHandles);
        $containerInstances = $this->groupByContainers($instancePages);

        $result = [];
        foreach ($containerInstances as $containerName => $instances) {
            $container = $this->widgetContainerFactory->create();
            $container->setContainer($containerName);

            $container->setWidgets($this->getWidgets($instances));
            $result[] = $container;
        }
        return $result;
    }

    private function groupByContainers(array $pageReferences): array
    {
        $containers = [];
        foreach ($pageReferences as $instance) {
            if (!isset($containers[$instance['block_reference']])) {
                $containers[$instance['block_reference']] = [];
            }
            $containers[$instance['block_reference']][] = $instance['instance_id'];
        }
        return $containers;
    }

    protected function getWidgetInstances($instanceIds): Collection
    {
        $widgets = $this->instanceCollectionFactory->create();
        $widgets
            ->addFieldToFilter('instance_id', $instanceIds)
            ->addFieldToFilter('instance_type', Banner::class);
        return $widgets;
    }

    private function getWidgets($instanceIds): array
    {
        $instances = $this->getWidgetInstances($instanceIds);
        if (!$instances->getSize()) {
            return [];
        }

        return $instances->walk(function (WidgetInstance $instance) {
            $widget = $this->widgetFactory->create();
            $widget->setBanners($this->getWidgetBanners($instance));
            return $widget;
        });
    }

    private function getWidgetBanners(WidgetInstance $widgetInstance)
    {
        $bannersConfig = $this->getBannersConfig($widgetInstance);
        $banners = $this->bannerCollectionFactory->create();
        $banners
            ->filterByIds(array_keys($bannersConfig))
            ->filterByCustomersData()
            ->filterByActive();

        return $banners->walk(function ($banner) use ($bannersConfig) {
            $widgetBanner = $this->widgetBannerFactory->create();
            $widgetBanner
                ->setBanner($banner)
                ->setPosition((int) $bannersConfig[$banner->getId()] ?? 0);
            return $widgetBanner;
        });
    }

    private function getBannersConfig(WidgetInstance $widgetInstance): array
    {
        $parameters = unserialize($widgetInstance->getData('widget_parameters'));
        if (!isset($parameters['banners'])) {
            return [];
        }

        return (array) \Zend\Json\Decoder::decode(
            $parameters['banners'], \Zend\Json\Json::TYPE_ARRAY
        );
    }
}