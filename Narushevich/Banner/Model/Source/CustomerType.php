<?php

namespace Narushevich\Banner\Model\Source;

use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;

/**
 * Class CustomerType
 *
 * @package Narushevich\Banner\Model\Source
 */
class CustomerType implements \Magento\Framework\Option\ArrayInterface
{
    const ANY_CUSTOMER_LABEL = 'Any customer';
    const ANY_CUSTOMER_VALUE = 0;
    const NEW_CUSTOMER_LABEL = 'New customer';
    const NEW_CUSTOMER_VALUE = 1;
    const WITH_ORDERS_CUSTOMER_LABEL = 'Customer with orders';
    const WITH_ORDERS_CUSTOMER_VALUE = 2;

    public function toOptionArray() : array
    {
        return [
            [
                'value' => self::ANY_CUSTOMER_VALUE,
                'label' => __(self::ANY_CUSTOMER_LABEL)
            ],
            [
                'value' => self::NEW_CUSTOMER_VALUE,
                'label' => __(self::NEW_CUSTOMER_LABEL)
            ],
            [
                'value' => self::WITH_ORDERS_CUSTOMER_VALUE,
                'label' => __(self::WITH_ORDERS_CUSTOMER_LABEL)
            ]
        ];
    }

    public function toArray() : array
    {
        [
            self::ANY_CUSTOMER_VALUE         => self::ANY_CUSTOMER_LABEL,
            self::NEW_CUSTOMER_VALUE         => self::NEW_CUSTOMER_LABEL,
            self::WITH_ORDERS_CUSTOMER_VALUE => self::WITH_ORDERS_CUSTOMER_LABEL
        ];
    }
}
