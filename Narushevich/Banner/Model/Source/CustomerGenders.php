<?php

namespace Narushevich\Banner\Model\Source;

use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;

/**
 * Class CustomerGenders
 *
 * @package Narushevich\Banner\Model\Source
 */
class CustomerGenders implements \Magento\Framework\Option\ArrayInterface
{
    const ANY_GENDER_LABEL = 'Any gender';
    const ANY_GENDER_VALUE = 0;
    const MAN_GENDER_LABEL = 'Man';
    const MAN_GENDER_VALUE = 1;
    const WOMAN_GENDER_LABEL = 'Woman';
    const WOMAN_GENDER_VALUE = 2;

    public function toOptionArray() : array
    {
        return [
            [
                'value' => self::ANY_GENDER_VALUE,
                'label' => __(self::ANY_GENDER_LABEL)
            ],
            [
                'value' => self::WOMAN_GENDER_VALUE,
                'label' => __(self::WOMAN_GENDER_LABEL)
            ],
            [
                'value' => self::MAN_GENDER_VALUE,
                'label' => __(self::MAN_GENDER_LABEL)
            ]
        ];
    }

    public function toArray() : array
    {
        [
            self::ANY_GENDER_VALUE   => self::ANY_GENDER_LABEL,
            self::WOMAN_GENDER_VALUE => self::WOMAN_GENDER_LABEL,
            self::MAN_GENDER_VALUE   => self::MAN_GENDER_LABEL
        ];
    }
}
