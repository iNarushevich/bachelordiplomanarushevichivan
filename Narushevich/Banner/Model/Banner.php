<?php
namespace Narushevich\Banner\Model;

use Magento\Framework\Model\AbstractModel;
use Narushevich\Banner\Api\Data\BannerInterface;
use Narushevich\Banner\Model\ResourceModel\Banner as BannerResource;

/**
 * Class Banner
 *
 * @package Narushevich\Banner\Model
 */
class Banner extends AbstractModel implements BannerInterface
{
    protected function _construct()
    {
        $this->_init(BannerResource::class);
    }

    public function getImageAttributes()
    {
        return [
            self::GENERAL_IMAGE,
            self::TABLET_IMAGE,
            self::MOBILE_IMAGE,
            self::PACKSHOT_IMAGE,
            self::PLACEHOLDER_IMAGE,
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return (string) $this->getData(self::NAME);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return (string) $this->getData(self::TITLE);
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return (string) $this->getData(self::SUBTITLE);
    }

    /**
     * @return string
     */
    public function getButtonLabel(): string
    {
        return (string) $this->getData(self::BUTTON_LABEL);
    }

    /**
     * @return string
     */
    public function getRemoveButton(): string
    {
        return (string) $this->getData(self::REMOVE_BUTTON);
    }

    /**
     * @return string
     */
    public function getFrontendClass(): string
    {
        return (string) $this->getData(self::FRONTEND_CLASS);
    }

    /**
     * @return string
     */
    public function getTextColor(): string
    {
        return (string) $this->getData(self::TEXT_COLOR);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return (string) $this->getData(self::URL);
    }

    /**
     * @return boolean
     */
    public function getIsUrlExternal(): bool
    {
        return (boolean) $this->getData(self::URL_IS_EXTERNAL);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return (string) $this->getData(self::CONTENT);
    }

    /**
     * @return int
     */
    public function getBannerId(): int
    {
        return (int) $this->getData(self::BANNER_ID);
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return (string) $this->getData(self::GENERAL_IMAGE);
    }

    /**
     * @return string
     */
    public function getMobileImage(): string
    {
        return (string) $this->getData(self::MOBILE_IMAGE);
    }

    /**
     * @return string
     */
    public function getTableImage(): string
    {
        return (string) $this->getData(self::TABLET_IMAGE);
    }

    /**
     * @return string
     */
    public function getPackshotImage(): string
    {
        return (string) $this->getData(self::PACKSHOT_IMAGE);
    }

    /**
     * @return string
     */
    public function getPlaceholderImage(): string
    {
        return (string) $this->getData(self::PLACEHOLDER_IMAGE);
    }
}