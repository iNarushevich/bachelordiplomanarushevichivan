<?php
namespace Narushevich\Banner\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Api\Data\StoreConfigInterface;

/**
 * Class Image
 *
 * @package Narushevich\Banner\Helper
 */
class Image extends AbstractHelper
{
    const DEFAULT_IMAGE_QUALITY = 100;

    private $imageHelper;
    private $config;

    public function __construct(
        Context $context,
        \Narushevich\MediaImage\Helper\Image $imageHelper,
        StoreConfigInterface $storeConfig
    ) {
        parent::__construct($context);
        $this->imageHelper = $imageHelper;
        $this->config = $storeConfig;
    }

    public function getUrl(string $path, string $imageId, string $type = 'image'): string
    {
        return $this->imageHelper
            ->init("banner/{$type}/{$path}", $imageId)
            ->setQuality($this->getImageQuality())
            ->getUrl();
    }

    private function getImageQuality(): int
    {
        $storeConfigImgQuality = $this->scopeConfig->getValue('dev/banners/img_quality');
        return (int) ($storeConfigImgQuality ?? self::DEFAULT_IMAGE_QUALITY);
    }
}