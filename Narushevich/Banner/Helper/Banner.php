<?php

namespace Narushevich\Banner\Helper;

use Magento\Framework\Exception\LocalizedException;
use Narushevich\Banner\Api\Data\BannerInterface;

/**
 * Class Banner
 *
 * @package Narushevich\Banner\Helper
 */
class Banner extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $imageUploaderFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ImageUploaderFactory $imageUploaderFactory
    ) {
        parent::__construct($context);
        $this->imageUploaderFactory = $imageUploaderFactory;
    }

    /**
     * Get Image uploader for image type
     *
     * @param string $type
     * @throws LocalizedException
     * @return \Magento\Catalog\Model\ImageUploader
     */
    public function getImageUploader($type)
    {
        switch ($type) {
            case BannerInterface::GENERAL_IMAGE:
                $imgType = $type;
                break;
            case BannerInterface::TABLET_IMAGE:
                $imgType = $type;
                break;
            case BannerInterface::MOBILE_IMAGE:
                $imgType = $type;
                break;
            case BannerInterface::PACKSHOT_IMAGE:
                $imgType = $type;
                break;
            case BannerInterface::PLACEHOLDER_IMAGE:
                $imgType = $type;
                break;
            default:
                throw new LocalizedException(__('image type not define'));
        }
        $tmpPath  = 'banner' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $imgType;
        $basePath = 'banner' . DIRECTORY_SEPARATOR . $imgType;
        return $this->imageUploaderFactory->create([
            'baseTmpPath'       => $tmpPath,
            'basePath'          => $basePath,
            'allowedExtensions' => ['jpg', 'jpeg', 'gif', 'png'],
        ]);
    }
}
