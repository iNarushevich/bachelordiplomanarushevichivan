<?php

namespace Narushevich\Banner\Api;

use Narushevich\Banner\Api\Data\BannerInterface;

/**
 * Interface for banner repository
 *
 * @package Narushevich\Banner\Api
 */
interface BannerRepositoryInterface
{
    /**
     * @param int|null $id
     * @return BannerInterface
     */
    public function get($id): BannerInterface;

    /**
     * @param BannerInterface $banner
     * @return void
     */
    public function save(BannerInterface $banner);

    /**
     * @param BannerInterface $banner
     * @return void
     */
    public function delete(BannerInterface $banner);
}
