<?php

namespace Narushevich\Banner\Api\Data;

/**
 * Interface for banner interface
 *
 * @package Narushevich\Banner\Api\Data
 */
interface BannerInterface
{
    const
        BANNER_ID         = 'banner_id',
        NAME              = 'name',
        TITLE             = 'title',
        SUBTITLE          = 'subtitle',
        BUTTON_LABEL      = 'button_label',
        REMOVE_BUTTON     = 'remove_button',
        FRONTEND_CLASS    = 'frontend_class',
        TEXT_COLOR        = 'text_color',
        URL               = 'url',
        URL_IS_EXTERNAL   = 'url_is_external',
        CONTENT           = 'content',
        GENERAL_IMAGE     = 'image',
        TABLET_IMAGE      = 'tablet_image',
        MOBILE_IMAGE      = 'mobile_image',
        PACKSHOT_IMAGE    = 'packshot_image',
        PLACEHOLDER_IMAGE = 'placeholder_image';

    /**
     * @return string
     */
    public function getName(): string;
    /**
     * @return string
     */
    public function getTitle(): string;
    /**
     * @return string
     */
    public function getSubtitle(): string;
    /**
     * @return string
     */
    public function getButtonLabel(): string;

    /**
     * @return string
     */
    public function getRemoveButton(): string;

    /**
     * @return string
     */
    public function getFrontendClass(): string;

    /**
     * @return string
     */
    public function getTextColor(): string;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return boolean
     */
    public function getIsUrlExternal(): bool;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @return int
     */
    public function getBannerId(): int;

    /**
     * @return string
     */
    public function getImage(): string;

    /**
     * @return string
     */
    public function getMobileImage(): string;

    /**
     * @return string
     */
    public function getTableImage(): string;

    /**
     * @return string
     */
    public function getPackshotImage(): string;

    /**
     * @return string
     */
    public function getPlaceholderImage(): string;
}
