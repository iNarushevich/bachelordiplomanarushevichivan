<?php

namespace Narushevich\Banner\Api\Data;

/**
 * Widget container interface
 *
 * Interface WidgetContainerInterface
 * @package Narushevich\Banner\Api\Data
 */
interface WidgetContainerInterface
{
    const CONTAINER = 'container';
    const WIDGETS = 'widgets';

    /**
     * @return string
     */
    public function getContainer(): string;

    /**
     * @return \Narushevich\Banner\Api\Data\WidgetInterface[]
     */
    public function getWidgets(): array;

    /**
     * @param string $container
     *
     * @return mixed
     */
    public function setContainer(string $container);

    /**
     * @param \Narushevich\Banner\Api\Data\WidgetInterface[] $widgets
     *
     * @return mixed
     */
    public function setWidgets(array $widgets);
}