<?php
namespace Narushevich\Banner\Api\Data;

/**
 * Widget interface
 *
 * @package Narushevich\Banner\Api\Data
 */
interface WidgetInterface
{
    const BANNERS = 'banners';

    /**
     * @return \Narushevich\Banner\Api\Data\WidgetBannerInterface[]
     */
    public function getBanners(): array;

    /**
     * @param \Narushevich\Banner\Api\Data\WidgetBannerInterface[] $banners
     * @return mixed
     */
    public function setBanners(array $banners);
}