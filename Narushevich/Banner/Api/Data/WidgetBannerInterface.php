<?php

namespace Narushevich\Banner\Api\Data;

/**
 * Widget banner interface
 *
 * Interface WidgetBannerInterface
 * @package Narushevich\Banner\Api\Data
 */
interface WidgetBannerInterface
{
    const BANNER = 'banner';
    const POSITION = 'position';

    /**
     * @return \Narushevich\Banner\Api\Data\BannerInterface
     */
    public function getBanner(): BannerInterface;

    /**
     * @return int
     */
    public function getPosition(): int;

    /**
     * @param \Narushevich\Banner\Api\Data\BannerInterface $banner
     * @return mixed
     */
    public function setBanner(\Narushevich\Banner\Api\Data\BannerInterface $banner);

    /**
     * @param int $position
     * @return mixed
     */
    public function setPosition(int $position);
}