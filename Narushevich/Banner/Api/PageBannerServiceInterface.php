<?php

namespace Narushevich\Banner\Api;

/**
 * Interface for PageBannerService
 *
 * @package Narushevich\Banner\Api
 */
interface PageBannerServiceInterface
{
    /**
     * @param string[] $layoutHandles
     * @return \Narushevich\Banner\Api\Data\WidgetContainerInterface[]
     */
    public function getBanners(array $layoutHandles): array;
}
